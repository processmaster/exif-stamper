defmodule Exifstamper.PhotoController do
  use Exifstamper.Web, :controller

  def create(conn, %{"file" => base64_str, "name" => filename, "folder" => folder}) do

    # Decode base64 string into a file binary
    file_binary = base64_to_file(base64_str)

    # Extract EXIF data and generate caption
    {:ok, exif} = Exexif.exif_from_jpeg_buffer(file_binary)
    caption = caption_from_exif(exif)

    # Composite EXIF data on image
    exifstamped_image = caption_image(file_binary, caption)

    # Upload the file to S3
    s3_bucket = Application.get_env(:exifstamper, :s3)[:bucket]
    s3_filename = "#{folder}/#{filename}"
    {:ok, _} = 
      ExAws.S3.put_object(s3_bucket, s3_filename, File.read!(exifstamped_image), [acl: :public_read])
      |> ExAws.request()

    photo = %{
      url: "https://#{s3_bucket}.s3.amazonaws.com/#{s3_filename}"
    }
    render conn, photo: photo
  end

  defp base64_to_file(base64_str) do
    # The next two lines are only necessary if the base64 prefix is present:
    #{start, length} = :binary.match(base64_str, ";base64,")
    #raw = :binary.part(base64_str, start + length, byte_size(base64_str) - start - length)
    Base.decode64! String.trim(base64_str), padding: false
  end

  defp caption_from_exif(exif) do
    datetime = exif[:exif].datetime_digitized
    {lat, lng} = latlng_from_exif_gps(exif[:gps])
    "#{datetime}\n#{lat}, #{lng}"
  end

  defp caption_image(file_binary, caption) do

    # Save base64 data to temporary file because ImageMagick
    # will only process images on the file system.
    {:ok, tmpfile} = Briefly.create
    File.write!(tmpfile, file_binary)

    System.cmd("convert",
      [
        tmpfile,
        "-font", "DejaVu-Sans",
        "-fill", "white",
        "-pointsize", "16",
        "-gravity", "SouthWest",
        "-draw", "text +15,+15 '#{caption}'",
        tmpfile
      ]
    )
    tmpfile
  end

  defp latlng_from_exif_gps(gps) do
    lat = coord_to_dms gps.gps_latitude,  gps.gps_latitude_ref
    lng = coord_to_dms gps.gps_longitude, gps.gps_longitude_ref
    {lat, lng}
  end

  defp coord_to_dms([degrees, minutes, seconds], ref) do
    "#{degrees}° #{minutes}’ #{seconds}” #{ref}"
  end
end
