defmodule Exifstamper.PhotoView do
  use Exifstamper.Web, :view

  #def render("create.json", %{photo: photo}) do
  #  photo.url
  #end

  def render("create.text", %{photo: photo}) do
    photo.url
  end
end
