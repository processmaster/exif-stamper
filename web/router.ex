defmodule Exifstamper.Router do
  use Exifstamper.Web, :router

  pipeline :api do
    plug :accepts, ["text"]
  end

  scope "/api", Exifstamper do
    pipe_through :api

    #resources "/photos", PhotoController
    #get "/photos", PhotoController, :index
    post "/photos", PhotoController, :create
  end
end
