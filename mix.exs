defmodule Exifstamper.Mixfile do
  use Mix.Project

  def project do
    [app: :exifstamper,
     version: "0.0.1",
     elixir: "~> 1.5.2",
     elixirc_paths: elixirc_paths(Mix.env),
     compilers: [:phoenix, :gettext] ++ Mix.compilers,
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps()]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Exifstamper, []},
      applications: [
        :phoenix,
        :phoenix_pubsub,
        :cowboy,
        :logger,
        :gettext,
        :ex_aws,
        :hackney,
        :poison,
        :briefly
      ]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "web", "test/support"]
  defp elixirc_paths(_),     do: ["lib", "web"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.3.0"},
      {:phoenix_pubsub, "~> 1.0.2"},
      {:gettext, "~> 0.13.1"},
      {:cowboy, "~> 1.1.2"},

      # Parse EXIF data from image uploads
      {:exexif, "~> 0.0.4"},

      # Temp files for ImageMagick processing
      {:briefly, "~> 0.3"},

      # Handle file uploads and S3 storage
      #{:uuid, "~> 1.1"},
      {:ex_aws, "~> 1.1"},
      {:poison, "~> 3.1"},
      {:hackney, "~> 1.9"}
    ]
  end
end
