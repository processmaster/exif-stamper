# base image elixer to start with
FROM elixir:latest

# install hex package manager
RUN mix local.hex --force

# install the latest phoenix 
RUN mix archive.install https://github.com/phoenixframework/archives/raw/master/phoenix_new.ez --force

# Install Ubuntu dependencies
RUN apt-get update
RUN apt-get install -y -q imagemagick

## install node
#RUN curl -sL https://deb.nodesource.com/setup_6.x -o nodesource_setup.sh  
#RUN bash nodesource_setup.sh  
#RUN apt-get install nodejs

# create app folder
ADD . /app
WORKDIR /app

## setting the port and the environment (prod = PRODUCTION!)
#ENV MIX_ENV=prod  
#ENV PORT=4000

# install dependencies (production only)
RUN mix local.rebar --force  
RUN mix deps.get --only prod  
RUN mix compile

## install node dependencies
#RUN npm install  
#RUN node node_modules/brunch/bin/brunch build

# run phoenix in *dev* mode on port 4000
CMD ["mix", "phx.server"]
